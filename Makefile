CC = emcc
CFLAGS = -O2

pages:
	@mkdir -p public/hello/file
	@mkdir -p public/hello/sdl
	@cp index.html public/
	@${CC} ${CFLAGS} tests/hello_world.c -o public/hello/index.html
	@${CC} ${CFLAGS} tests/hello_world_sdl.cpp -o public/hello/sdl/index.html
	@${CC} ${CFLAGS} tests/hello_world_file.cpp -o public/hello/file/index.html --preload-file tests/hello_world_file.txt

